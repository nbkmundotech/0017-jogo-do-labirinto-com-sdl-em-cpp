build/labirinto: build build/Main.o build/Jogo.o build/MapaDoJogo.txt
	g++ -o build/labirinto build/Main.o build/Jogo.o -lSDL2 -lSDL2_ttf -lSDL2_image

build:
	mkdir build

build/Main.o: Main.cpp
	g++ -c Main.cpp -o build/Main.o

build/Jogo.o: Jogo.cpp
	g++ -c Jogo.cpp -o build/Jogo.o

build/MapaDoJogo.txt: MapaDoJogo.txt
	cp MapaDoJogo.txt build/MapaDoJogo.txt
