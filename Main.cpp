#include "Jogo.h"

int main() {
  Jogo jogo;

  jogo.iniciar();
  jogo.carregarFontes();
  jogo.criarJanela();
  jogo.configurarCorJogador();

  jogo.carregarMapa();
  jogo.imprimirMapa();
  jogo.configurarSuperficieDoMapaDeJogo();

  jogo.rodar();

  jogo.desligar();

  return 0;
}
