#include <iostream>
#include <fstream>
#include <SDL2/SDL_image.h>

#include "Jogo.h"
#include "TipoDeAzulejo.h"

Jogo::Jogo() {
  posicaoEntrada = {0, 0, TAMANHO_DO_AZULEJO, TAMANHO_DO_AZULEJO};
  posicaoSaida = {0, 0, TAMANHO_DO_AZULEJO, TAMANHO_DO_AZULEJO};
  posicaoJogador = {
    posicaoEntrada.x,
    posicaoEntrada.y,
    32,
    32
  };
  acabou = false;
}

void Jogo::iniciar() {
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Nao deu certo iniciar o SDL: %s", SDL_GetError());
    exit(1);
  }

  if (TTF_Init() != 0) {
    SDL_Log("Nao deu certo iniciar o SDL TTF: %s", TTF_GetError());
    exit(2);
  }

  // operacoes logicas
  // OR:    0 e 1 = 1;
  //        1 e 0 = 1;
  //        0 e 0  = 0 ;
  //        1 e 1 = 1
  // 0100 | 1000   =  1100
  //
  // AND:  0 e 0 = 0;
  //       0 e 1 = 0;
  //       1 e 0 = 0;
  //       1 e 1 = 1;
  int flags = IMG_INIT_PNG | IMG_INIT_PNG;
  int initted = IMG_Init(flags);
  // 1100 & 1100 = 1100
  if ( (initted & flags) != flags) {
    SDL_Log("Nao deu certo carregar o SDL image: %s", IMG_GetError());
    exit(5);
  }
}

void Jogo::carregarFontes() {
  font = TTF_OpenFont("/usr/share/fonts/truetype/open-sans/OpenSans-Regular.ttf", 24);
  if (!font) {
    SDL_Log("Erro ao carregar fonte: %s", TTF_GetError());
    exit(3);
  }
}

void Jogo::criarJanela() {
  window = SDL_CreateWindow(
    "Ola Mundo",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    WINDOW_WIDTH,
    WINDOW_HEIGHT,
    0
  );
  if (window == NULL) {
    std::cerr << "Nao deu certo criar a janela: " << SDL_GetError() << std::endl;
    exit(2);
  }

  windowSurface = SDL_GetWindowSurface(window);
}

void Jogo::configurarCorJogador() {
  corJogador = SDL_MapRGB(windowSurface->format, 128, 128, 128);
}

void Jogo::desligar() {
  SDL_FreeSurface(mapaDeJogo);
  SDL_DestroyWindow(window);

  IMG_Quit();
  TTF_Quit();
  SDL_Quit();
}

// 1. Carregar o mapa
// 20 * 15 = 300 elementos (ids de azulejos)
void Jogo::carregarMapa() {
  std::ifstream mapaDoJogo("MapaDoJogo.txt");

  if (mapaDoJogo.is_open()) {
    int linhaDaEntrada, colunaDaEntrada;
    mapaDoJogo >> linhaDaEntrada >> colunaDaEntrada;
    posicaoEntrada.x = colunaDaEntrada * TAMANHO_DO_AZULEJO;
    posicaoEntrada.y = linhaDaEntrada * TAMANHO_DO_AZULEJO;
    int linhaDaSaida;
    int colunaDaSaida;
    mapaDoJogo >> linhaDaSaida >> colunaDaSaida;
    posicaoSaida.x = colunaDaSaida * TAMANHO_DO_AZULEJO;
    posicaoSaida.y = linhaDaSaida * TAMANHO_DO_AZULEJO;

    // ler o id do proximo tijolo
    int idDoAzulejo;
    int linha = 0;
    int coluna = 0;
    while (mapaDoJogo >> idDoAzulejo) {
      azulejos[linha][coluna] = idDoAzulejo;

      coluna++;
      if (coluna == 20) {
        coluna = 0;
        linha++;
      }
    }

    // fecha o arquivo
    mapaDoJogo.close();
  }
}

void Jogo::imprimirMapa() {
  for (int linha = 0; linha < 15; linha++) {
    for (int coluna = 0; coluna < 20; coluna++) {
      std::cout << azulejos[linha][coluna];

      if (coluna == 19) {
        std::cout << std::endl;
      }
      else {
        std::cout << " ";
      }
    }
  }
}

void Jogo::pintarEntrada() {
  Uint32 cor = SDL_MapRGB(mapaDeJogo->format, 128, 255, 128);
  SDL_FillRect(mapaDeJogo, &posicaoEntrada, cor);
}

void Jogo::pintarSaida() {
  Uint32 cor = SDL_MapRGB(mapaDeJogo->format, 255, 128, 128);
  SDL_FillRect(mapaDeJogo, &posicaoSaida, cor);
}

// 2. Criar a superficie do mapa baseado nos ids dos azulejos
SDL_Surface* Jogo::criarSuperficieDoMapaDeJogo() {
  SDL_Surface* superficie = SDL_CreateRGBSurface(
    0,
    640,
    480,
    32,
    0,
    0,
    0,
    0
  );

  // pintar quadradinhos na superficie
  SDL_Rect retangulo;
  retangulo.w = 32;
  retangulo.h = 32;

  for (int linha = 0; linha < 15; linha++) {
    for (int coluna = 0; coluna < 20; coluna++) {
      retangulo.x = coluna * 32;
      retangulo.y = linha * 32;

      int idDoAzulejo = azulejos[linha][coluna];
      Uint32 corDoAzulejo;
      if (idDoAzulejo == 0) {
        // pode andar
        corDoAzulejo = SDL_MapRGB(superficie->format, 44, 44, 44);
      }
      else if (idDoAzulejo == 1) {
        // muro (bloqueado)
        corDoAzulejo = SDL_MapRGB(superficie->format, 255, 255, 255);
      }

      SDL_FillRect(superficie, &retangulo, corDoAzulejo);
    }
  }

  return superficie;
}

void Jogo::configurarSuperficieDoMapaDeJogo() {
  mapaDeJogo = criarSuperficieDoMapaDeJogo();
  pintarEntrada();
  pintarSaida();
}

void Jogo::rodar() {
  bool sair = false;
  Uint32 tempoInicial = SDL_GetTicks(); // e.g. 1234 ms
  bool mostrarMensagemDeObjetivo = true;

  SDL_Color corDaFonte = {255, 0, 0};
  SDL_Surface* superficieDaMensagemDeObjetivo = TTF_RenderUTF8_Solid(font, "Objetivo: atravessar o labirinto e encontrar a saída", corDaFonte);
  SDL_Rect destinoDaMensagemDeObjetivo = {
    8,
    8,
    superficieDaMensagemDeObjetivo->w,
    superficieDaMensagemDeObjetivo->h
  };

  SDL_Surface* superficieDaImagemInicial = SDL_LoadBMP("correndo.bmp");
  if (!superficieDaImagemInicial) {
    SDL_Log("Erro ao carregar imagem: %s", SDL_GetError());
    exit(4);
  }
  SDL_Rect destinoDaImagemInicial = {
    (WINDOW_WIDTH - superficieDaImagemInicial->w ) / 2,
    (WINDOW_HEIGHT - superficieDaImagemInicial->h) / 2,
    0,
    0
  };

  SDL_Surface* superficieDaImagemFinal = IMG_Load("trofeu.png");
  if (!superficieDaImagemInicial) {
    SDL_Log("Erro ao carregar imagem: %s", IMG_GetError());
    exit(4);
  }
  SDL_Rect destinoDaImagemFinal = {
    (WINDOW_WIDTH - superficieDaImagemFinal->w) / 2,
    (WINDOW_HEIGHT - superficieDaImagemFinal->h) / 2,
    0,
    0
  };

  while (!sair) {
    while (SDL_PollEvent(&event) != 0) {
      if (event.type == SDL_QUIT) {
        sair = true;
      }
      else if (event.type == SDL_KEYDOWN) {
        if (!acabou) {
          processarEntradaDoJogador();
        }
      }
    }

    if (!acabou && jogoDeveAcabar()) {
      acabou = true;
    }

    SDL_BlitSurface(mapaDeJogo, NULL, windowSurface, NULL);
    SDL_FillRect(windowSurface, &posicaoJogador, corJogador);

    Uint32 tempoAgora = SDL_GetTicks(); // e.g. 11235
    if (tempoAgora - tempoInicial > 10000) {
      mostrarMensagemDeObjetivo = false;
    }

    if (mostrarMensagemDeObjetivo) {
      SDL_BlitSurface(superficieDaMensagemDeObjetivo, NULL, windowSurface, &destinoDaMensagemDeObjetivo);
      SDL_BlitSurface(superficieDaImagemInicial, NULL, windowSurface, &destinoDaImagemInicial);
    }
    if (acabou) {
      SDL_BlitSurface(superficieDaImagemFinal, NULL, windowSurface, &destinoDaImagemFinal);
    }

    SDL_UpdateWindowSurface(window);
  }

  SDL_FreeSurface(superficieDaMensagemDeObjetivo);
  SDL_FreeSurface(superficieDaImagemInicial);
  SDL_FreeSurface(superficieDaImagemFinal);
}

int Jogo::mapearCoordenadaParaIdDoAzulejo(SDL_Rect posicao) {
  int tamanhoDoAzulejo = 32;
  return azulejos[posicao.y / tamanhoDoAzulejo][posicao.x / tamanhoDoAzulejo];
}

bool Jogo::azulejoForAtravessavel(int idDoAzulejo) {
  if (idDoAzulejo == Chao) {
    return true;
  }
  else if (idDoAzulejo == Muro) {
    return false;
  }

  return false;
}

void Jogo::processarEntradaDoJogador() {
  SDL_Rect novaPosicao = posicaoJogador;

  if (event.key.keysym.sym == SDLK_RIGHT) {
    if (posicaoJogador.x + 32 < WINDOW_WIDTH) {
      novaPosicao.x += 32;
      int idDoProximoAzulejo = mapearCoordenadaParaIdDoAzulejo(novaPosicao);
      if (azulejoForAtravessavel(idDoProximoAzulejo)) {
        posicaoJogador.x = posicaoJogador.x + 32;
      }
    }
  }
  else if (event.key.keysym.sym == SDLK_DOWN) {
    if (posicaoJogador.y + 32 < WINDOW_HEIGHT) {
      novaPosicao.y += 32;
      int idDoProximoAzulejo = mapearCoordenadaParaIdDoAzulejo(novaPosicao);
      if (azulejoForAtravessavel(idDoProximoAzulejo)) {
        posicaoJogador.y = posicaoJogador.y + 32;
      }
    }
  }
  else if (event.key.keysym.sym == SDLK_LEFT) {
    if (posicaoJogador.x - 32 >= 0) {
      novaPosicao.x -= 32;
      int idDoProximoAzulejo = mapearCoordenadaParaIdDoAzulejo(novaPosicao);
      if (azulejoForAtravessavel(idDoProximoAzulejo)) {
        posicaoJogador.x = posicaoJogador.x - 32;
      }
    }
  }
  else if (event.key.keysym.sym == SDLK_UP) {
    if (posicaoJogador.y - 32 >= 0) {
      novaPosicao.y -= 32;
      int idDoProximoAzulejo = mapearCoordenadaParaIdDoAzulejo(novaPosicao);
      if (azulejoForAtravessavel(idDoProximoAzulejo)) {
        posicaoJogador.y = posicaoJogador.y - 32;
      }
    }
  }
}

bool Jogo::jogoDeveAcabar() {
  return posicaoJogador.x == posicaoSaida.x && posicaoJogador.y == posicaoSaida.y;
}
