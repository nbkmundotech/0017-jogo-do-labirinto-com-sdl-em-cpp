#ifndef __JOGO__
#define __JOGO__

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

class Jogo {
  SDL_Window* window;
  SDL_Surface* windowSurface;
  int WINDOW_WIDTH = 640;
  int WINDOW_HEIGHT = 480;
  int TAMANHO_DO_AZULEJO = 32;
  Uint32 corJogador;
  int azulejos[15][20];
  SDL_Rect posicaoEntrada;
  SDL_Rect posicaoSaida;
  SDL_Rect posicaoJogador;
  SDL_Surface* mapaDeJogo;
  SDL_Event event;
  TTF_Font* font;
  bool acabou;
public:
  Jogo();
  void iniciar();
  void carregarFontes();
  void criarJanela();
  void configurarCorJogador();
  void desligar();
  void carregarMapa();
  void imprimirMapa();
  void configurarSuperficieDoMapaDeJogo();
  void pintarEntrada();
  void pintarSaida();
  SDL_Surface* criarSuperficieDoMapaDeJogo();
  void rodar();
  void processarEntradaDoJogador();
  int mapearCoordenadaParaIdDoAzulejo(SDL_Rect posicao);
  bool azulejoForAtravessavel(int idDoAzulejo);
  bool jogoDeveAcabar();
};

#endif
